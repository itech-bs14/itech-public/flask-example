# Start service locally
* Clone project 
* Install requirements 
* Run flask service ($ export FLASK_APP=app.py / $ export FLASK_ENV=development / $ flask run)

# Call service
* http://localhost:5000
* http://localhost:5000/auswertung
* http://localhost:5000/bucketlist

# Used technologies
* Python, Flask, Gitlab

# Flask as a microframework
* The “micro” in microframework means Flask aims to keep the core simple but extensible.
* Flask won’t make many decisions for you, such as what database to use.
* Those decisions that it does make, such as what templating engine to use, are easy to change.
* Everything else is up to you, so that Flask can be everything you need and nothing you don’t.
* By default, Flask does not include a database abstraction layer, form validation or anything else where different libraries already exist that can handle that.
* Instead, Flask supports extensions to add such functionality to your application as if it was implemented in Flask itself. Numerous extensions provide database integration, form validation, upload handling, various open authentication technologies, and more. Flask may be “micro”, but it’s ready for production use on a variety of needs.
* ![Install flask](https://flask.palletsprojects.com/en/2.3.x/installation/)

# What is Gitlab
* a web-based DevOps (Developement Operations) lifecycle tool
* provides a Git-repository manager
* providing wiki
* issue-tracking
* continuous integration and deployment pipeline features
* using an open-source license which covers the core functionality

# What is CSS?
* CSS stands for Cascading Style Sheets
* CSS describes how HTML elements are to be displayed on screen, paper, or in other media
* CSS saves a lot of work. It can control the layout of multiple web pages all at once
* External stylesheets are stored in CSS files
* ![further information and tutorials. ](https://www.w3schools.com/w3css/defaulT.asp)

# What is HTML?
* HTML is the standard markup language for Web pages.
* With HTML you can create your own Website.
* ![further information and tutorials:](https://www.w3schools.com/html/)
