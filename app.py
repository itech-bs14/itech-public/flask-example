from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)
app.debug = True

name_list = {"Accuse Someone of Farting In An Elevator","Block a Store’s Door At and Ask For the Secret Entry Password","Bring a Fishing Pole to an Aquarium","Call Someone To Tell Them You Can’t Talk Right Now","Convince Someone You Are From The Future","Create A Random Piece Of Art And Put It for Sale for $1,000,000","Glue Coins To A Street In The Crosswalk","Go Up To Strangers And Act Like Lifelong Friends"}

@app.route('/')
def main():
    return render_template('index.html')

@app.route('/auswertung',methods = ['POST', 'GET'])
def auswertung():
    if request.method == 'GET':
        first_name = request.args.get('fname')
        last_name = request.args.get('lname')
        return render_template('welcome.html', fname=first_name, lname=last_name)
    else:
        return redirect(url_for('main'))

@app.route('/bucketlist')
def bucketlist():
    return render_template('bucketlist.html', data=name_list, name="Michael")
